
/************************************************************************************/
/* Manages /auberge command */
const _manage_inn_roll = async (content, msg) => {
    // Split input into arguments
    let command = content.split(" ").map(function(item) {
      return item.trim();
    })
    
    if (command[0] == "/auberge" && command[1] )
    {
      msg["type"] = 0;
      msg["rollMode"] = "gmroll";
      var compendium = game.packs.find(p => p.collection === 'WH4-fr-translation.plats-dauberges');
      let rollList = [];
      await compendium.getIndex().then(index => rollList = index);
      //console.log("Got compendium...", rollList.length);
      for (var i=0; i< rollList.length; i++) {
        var rollTab = rollList[i];
        if ( rollTab.name.toLowerCase().includes(command[1].toLowerCase()) ) {
          let my_rollTable; 
          await compendium.getEntity(rollTab._id).then(mytab => my_rollTable = mytab);
          var result = my_rollTable.roll();
          console.log("RES: ", result[0], result[1] );
          msg.content = my_rollTable.name + " : " + result[1].text;
          //my_rollTable.draw();
          ChatMessage.create(msg);          
          return false;
        }
      }
    }
    if ( content.includes("/auberge") ) {
      msg["type"] = 0;
      msg["rollMode"] = "gmroll";
      msg["content"] = "Syntaxe : /auberge MOT_CLE, avec MOT_CLE parmi:<br>BoissonsBase, BoissonsFortes, Desserts, PlatsCommuns, PlatsExcellents, PlatsMaritimes, PlatsMédiocres, PlatsQualité,        PlatsRivières<br>Des raccourcis sont possibles avec une partie du nom : /auberge Base (correspond à BoissonBase) ou /auberge Mari (correspond à PlatsMaritimes), etc."      
      ChatMessage.create(msg);
      return false;
    }
}

/************************************************************************************/
/* Hook for specific command */
Hooks.on("chatMessage", (html, content, msg) => {
  
  if ( content.includes("/auberge") ) {
    return _manage_inn_roll( content, msg );
  }
} );

/************************************************************************************/
/* Additionnal hooks ready */
Hooks.once('ready', () => {

  const lang = game.settings.get('core', 'language');
  if ( lang == "fr" ) { 
    game.packs.forEach((pack, idx) => {
      if(!pack.translated && pack.entity === 'Actor') {
        let translations = {
              "label": pack.metadata.name,
              "mapping": { 
                "name": "name",
                "description": "details.biography.value",              
                "items": {
                  "path": "items", 
                  "converter": "bestiary_traits"
                },
                "characteristics": {
                  "path": "data.characteristics",
                  "converter": "npc_characteristics"
                }
              },
              "entries": [
              ]
            };    
        console.log("Actor compendium has been replaced !!!!");
        game.packs[idx] = new TranslatedCompendium(pack, translations);
      }
    } );
  }

} );
